<?php
/**
 * Created by PhpStorm.
 * User: Trainer 402
 * Date: 1/28/2017
 * Time: 2:43 PM
 */

namespace OOP;


abstract class AbstractClass
{
    // Force Extending class to define this method
    abstract protected function getMyValue();
    abstract protected function setMyValue($prefix);

    // Common method
    public function printOut() {
        print $this->getMyValue() . "\n";
    }
}

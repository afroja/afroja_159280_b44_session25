<?php
/**
 * Created by PhpStorm.
 * User: Trainer 402
 * Date: 1/28/2017
 * Time: 3:33 PM
 */

namespace OOP;


class Penguin extends Bird implements canSwim{
    public $name = "Penguin";

    public function swim()
    {
        echo "I can Swim<br>";
    }

}
